function setStory(url) {
    const imagen = document.getElementById("story");
    imagen.src = url;
 }

 function generarProgressBars(items) {
    const contenidoProgresBars = items.reduce((previo, currentValue) => {
        const { entry_id } = currentValue;
        const contenido = `${previo}<td><progress id="progress-bar-${entry_id}" value="0" max="100"></progress></td>`
        return contenido;
    }, '');
    document.getElementById('progresBarsContainer').innerHTML = contenidoProgresBars;
 }

 function startInfogramas(memes) {
    let memesCopy = [...memes];
    let contador = 0;
    let [memeSiguiente] = memesCopy;

    setInterval(() => {
        contador = contador + 1;
        if (contador >= 100) {
            // pasar a la siguiente imagen
            memeSiguiente = memesCopy.shift();
            if (memesCopy.length === 1) {
                memesCopy = [...memes];
                generarProgressBars(memesCopy);
            };

            setStory(memeSiguiente.asset_url);
            contador = 0;
        } else {
            window[`progress-bar-${memeSiguiente.entry_id}`].value = contador;
        }
    }, 10);
 }

  
 fetch('https://cmsapi.ameliarueda.com//endpoints/stories')
    .then(response => response.json())
    .then((data) => {
        const { memes, infografias } = data;
        const items = [...memes.map(({ entry_id, asset_url }) => ({ 
            entry_id: 'memes' + entry_id,
            asset_url
        })), ...infografias ];

        generarProgressBars(items);
        startInfogramas(items);
 })
 