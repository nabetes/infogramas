
(function() {
    fetch('http://localhost:8080/validateIdentity', {
        headers: {
            token: document.cookie.split('=')[1],
        }
    })
    .catch((error) => {
        debugger;
        location.href = 'http://localhost:3000/login.html'
    });

    window.customOnClick = (pageKey) => {
        fetch(`./${pageKey}.html`)
        .then(respose => respose.text())
        .then(htmlContenido => {
            const contenedor = document.getElementById('main_content');
            contenedor.innerHTML = htmlContenido;
    
            const element = document.createElement('script');
            element.setAttribute('src', `./${pageKey}.js`);
            contenedor.appendChild(element);
        });
    }
})();
