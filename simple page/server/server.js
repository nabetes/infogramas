const express = require('express');
const jsonwebtoken = require('jsonwebtoken');
const cors = require('cors');


const app = express();

const SESSION_ACTIVE = 'SESSION_ACTIVE';
const PRIVATE_KEY = 'super-private-key';

const PORT = 8080;
const USERS = [
    {
        "username": "johnny",
        "age": 25,
        "password": "123",
    }, { "username": "byron", "password": "123" }, { "username": "alyery", "password": "123" }];


// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());
app.use(express.json());
app.use(cors());

// create a route for the app
app.post('/login', (req, res) => {
    const { username, password } = req.body;

    const reject = () => {
        res.statusCode = 401;
        res.redirect('http://localhost:3000/login.html');
    };

    if (!username || !password) {
        reject();
        return;
    }

    var userData = USERS.find(user => user.username == username && user.password == password);
    if (!userData) {
        reject();
        return;
    }

    res.cookie('token', jsonwebtoken.sign(userData, PRIVATE_KEY));
    res.redirect('http://localhost:3000/index.html');
});


app.get('/validateIdentity', (req, res) => {
    const token = req.header('token');

    const onSuccess = () => {
        res.status = 400;
        res.end();
    };

    const onError = () => {
        res.status = 401;
        res.redirect('http://localhost:3000/login.html');
        res.end();
    };

    jsonwebtoken.verify(token, onSuccess, onError);
});

// make the server listen to requests
app.listen(PORT, () => {
    console.log(`Server running at: http://localhost:${PORT}/`);
});